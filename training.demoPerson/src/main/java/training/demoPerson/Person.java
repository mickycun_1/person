package training.demoPerson;

public class Person {

	private String name;
	private int id;

	public Person(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}


	@Override
	public String toString() {
		return String.format("ID : %d, Name : %s", this.getId(), this.getName());
	}
}
