package training.demoPerson;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

//import org.junit.Before;
import org.junit.Test;

public class testPerson {


	@Test
	public void testCreatePerson() {
		Person p = new Person(1, "Mick");
		assertNotNull(p);
		assertEquals(1, p.getId());
		assertEquals("Mick", p.getName());
		assertThat(p.toString().indexOf("Mick"), is(not(-1)));
	}
	
}
