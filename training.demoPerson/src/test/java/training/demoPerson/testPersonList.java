package training.demoPerson;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

public class testPersonList {


	@Test
	public void testCreateEmptyList() {
		PersonList pl = new PersonList();
		assertThat (pl.getPl().size(), is(0));
	}

	@Test
	public void addOnePersonSucceeds(){
		PersonList pl = new PersonList();
		pl.addPerson(new Person(1, "Martin"));
		assertThat(pl.getPl().size(), is(1));
	}
	
	@Test
	public void addOnePersonAndCheckAddedToList(){
		PersonList pl = new PersonList();
		pl.addPerson(new Person(1, "Mick"));
		Person[] persons = pl.getPersons();
		assertThat(persons.length, is(1));
		assertThat(persons[0].getName(), is("Mick"));
	}
	
	@Test
	public void removeOnePersonSucceeds(){
		PersonList pl = new PersonList();
		Person mick = new Person(1, "Mick");
		pl.addPerson(mick);
		Person p = pl.removePeople(mick);
		assertThat(pl.getSize(), is (0));
	}
	
	@Test
	public void returnTheSizeOfTheList(){
		PersonList pl = new PersonList();
		assertThat(pl.getPersons(), );
	}
	
	
	
	@Test
	public void sortListByName(){
		PersonList pl = new PersonList();
		Person person1 = new Person(3, "Michael");
		Person person2 = new Person(5, "Russell");
		Person person3 = new Person(6, "Jordy");
		Person person4 = new Person(7, "Odell");
		Person person5 = new Person(8, "Richard");
		pl.addPerson(person1);
		pl.addPerson(person2);
		pl.addPerson(person3);
		pl.addPerson(person4);
		pl.addPerson(person5);
		assertThat(pl.getPersons().length, is(6, "Jordy"));
	}
	@Test
	public void sortListByID(){
		PersonList pl = new PersonList();
		Person person1 = new Person(3, "Michael");
		Person person2 = new Person(5, "Russell");
		Person person3 = new Person(6, "Jordy");
		Person person4 = new Person(7, "Odell");
		Person person5 = new Person(8, "Richard");
		pl.addPerson(person1);
		pl.addPerson(person2);
		pl.addPerson(person3);
		pl.addPerson(person4);
		pl.addPerson(person5);
	}
	
	
}
